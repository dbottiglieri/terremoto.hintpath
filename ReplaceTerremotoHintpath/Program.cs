﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ReplaceTerremotoHintpath
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                DoStuff();
            } catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        static string GetTerremotoRoot(string path)
        {
            Console.WriteLine("Path is " + path);
            var pattern = @"<HintPath>.*\\(\.|[0-9]){8,}\\";
            return Regex.Match(File.ReadAllText(path), pattern).NextMatch().ToString().Replace("<HintPath>", "").TrimEnd('\\');
        }

        static void DoReplaces(string currentDirectory, string subDir)
        {
            var projfiles = Directory.GetFiles(currentDirectory + "\\" + subDir, "*.csproj", SearchOption.AllDirectories);

            foreach (var path in projfiles)
            {
                Console.WriteLine(path); // full path
                var pattern = @"<PropertyGroup>";
                var replacement = "<Import Project=\"..\\..\\..\\TerremotoDirectory.csproj\" />\n  <PropertyGroup>";
                var regex = new Regex(pattern);
                File.WriteAllText(path, regex.Replace(File.ReadAllText(path), replacement, 1));

                pattern = @"<HintPath>.*\\(\.|[0-9]){8,}\\";
                replacement = "<HintPath>$(TerremotoDirectory)\\";
                regex = new Regex(pattern);
                File.WriteAllText(path, regex.Replace(File.ReadAllText(path), replacement));
            }
        }

        static void DoStuff()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = @"C:\Git";

            string currentDirectory;
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                currentDirectory = fbd.SelectedPath;
                Console.WriteLine(string.Format("You are in {0}.", currentDirectory));

                if (Directory.GetFiles(currentDirectory, "*.sln", SearchOption.TopDirectoryOnly).Length == 0)
                {
                    Console.WriteLine("Fam this ain't no proj root fool");
                    Console.ReadKey();
                    return;
                }
                                
                var hintPath = GetTerremotoRoot(Directory.GetFiles(currentDirectory + "\\src", "*.csproj", SearchOption.AllDirectories)[0]);
                Console.WriteLine("Hint path is " + hintPath);

                // CREATE CSPROJ //

                File.AppendAllText(currentDirectory + "\\TerremotoDirectory.csproj", string.Format(@"<Project {0}>
  <PropertyGroup>
    <TerremotoDirectory>{1}</TerremotoDirectory>
  </PropertyGroup>
</Project>", "xmlns =\"http://schemas.microsoft.com/developer/msbuild/2003\"", hintPath));

                // CREATE CSPROJ //

                DoReplaces(currentDirectory, "src");
                DoReplaces(currentDirectory, "test");
            }
            Console.WriteLine("Done!");
            Console.ReadKey();
        }
    }
}
